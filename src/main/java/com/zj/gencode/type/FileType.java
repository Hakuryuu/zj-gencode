package com.zj.gencode.type;

import lombok.Getter;

/**
 * 每张表对应的输出文件类型
 * @author xi.yang
 * @create 2018-10-17 14:18
 **/
@Getter
public enum FileType {
    SQL(".sql"),
    TO(".java"),
    MAPPER("Mapper.java"),
    XML("Mapper.xml"),
    SERVICE("Service.java"),
    SERVICEIMPL("ServiceImpl.java"),
    ;
    /**
     * 文件名后缀-在to基础上加后缀作为文件名
     */
    private String fileSuffix;

    FileType(String fileSuffix) {
        this.fileSuffix = fileSuffix;
    }
}