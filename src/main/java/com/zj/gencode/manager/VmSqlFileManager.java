package com.zj.gencode.manager;

import com.zj.gencode.config.GenConfig;
import com.zj.gencode.model.excel.ExcelTable;
import com.zj.gencode.model.vm.VmSqlModel;
import com.zj.gencode.utils.velocity.VmUtil;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * sql文件输出
 *
 * @author xi.yang
 * @create 2018-12-14 17:48
 **/
public class VmSqlFileManager extends VmFileManager {
    @Override
    public void vmOut(List<ExcelTable> tables) {
        // 转换为sql模型
        List<VmSqlModel> vmSqlModels = tables.stream().map(VmSqlModel::new).collect(Collectors.toList());
        // 生成sql文件
        map.put("data",vmSqlModels);
        VmUtil.vmToFile("sql.vm",map, GenConfig.instance.getFileOutPath() + File.separator+"init.sql");
    }
}
