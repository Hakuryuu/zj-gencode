package com.zj.gencode.manager;

import com.zj.gencode.config.GenConfig;
import com.zj.gencode.model.excel.ExcelTable;
import com.zj.gencode.model.vm.VmServiceImplModel;
import com.zj.gencode.model.vm.VmServiceModel;
import com.zj.gencode.model.vm.VmSqlModel;
import com.zj.gencode.type.FileType;
import com.zj.gencode.utils.velocity.VmUtil;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 转换为serviceImpl模型并输出文件
 *
 * @author xi.yang
 * @create 2018-12-14 17:48
 **/
public class VmServiceImplFileManager extends VmFileManager {
    private FileType fileType = FileType.SERVICEIMPL;
    @Override
    public void vmOut(List<ExcelTable> tables) {
        // 转换为serviceImpl模型并输出文件
        tables.forEach(excelTable -> {
            VmServiceImplModel model = new VmServiceImplModel(excelTable);
            map.put("data", model);
            VmUtil.vmToFile(fileType.name().toLowerCase() + ".vm", map
                    , GenConfig.instance.getFileOutPath() + File.separator + fileType.name().toLowerCase() + File.separator + model.getClassName() + fileType.getFileSuffix());
        });
    }
}
