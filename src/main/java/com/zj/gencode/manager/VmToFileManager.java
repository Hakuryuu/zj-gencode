package com.zj.gencode.manager;

import com.zj.gencode.config.GenConfig;
import com.zj.gencode.model.excel.ExcelTable;
import com.zj.gencode.model.vm.VmToModel;
import com.zj.gencode.type.FileType;
import com.zj.gencode.utils.velocity.VmUtil;

import java.io.File;
import java.util.List;

/**
 * sql文件输出
 *
 * @author xi.yang
 * @create 2018-12-14 17:48
 **/
public class VmToFileManager extends VmFileManager {
    private FileType fileType = FileType.TO;
    @Override
    public void vmOut(List<ExcelTable> tables) {
        // 转换为To模型并输出文件
        tables.forEach(excelTable -> {
            VmToModel vmToModel = new VmToModel(excelTable);
            map.put("data", vmToModel);
            VmUtil.vmToFile(fileType.name().toLowerCase() + ".vm", map
                    , GenConfig.instance.getFileOutPath() + File.separator + fileType.name().toLowerCase() + File.separator + vmToModel.getClassName() + fileType.getFileSuffix());
        });
    }
}
