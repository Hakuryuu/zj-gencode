package com.zj.gencode.manager;

import com.zj.gencode.config.GenConfig;
import com.zj.gencode.model.excel.ExcelTable;
import com.zj.gencode.model.vm.VmSqlModel;
import com.zj.gencode.model.vm.VmToModel;
import com.zj.gencode.model.vm.VmXmlModel;
import com.zj.gencode.type.FileType;
import com.zj.gencode.utils.velocity.VmUtil;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * sql文件输出
 *
 * @author xi.yang
 * @create 2018-12-14 17:48
 **/
public class VmXmlFileManager extends VmFileManager {
    private FileType fileType = FileType.XML;
    @Override
    public void vmOut(List<ExcelTable> tables) {
        // 转换为To模型并输出文件
        tables.forEach(excelTable -> {
            VmXmlModel model = new VmXmlModel(excelTable);
            map.put("data", model);
            VmUtil.vmToFile(fileType.name().toLowerCase() + ".vm", map
                    , GenConfig.instance.getFileOutPath() + File.separator + fileType.name().toLowerCase() + File.separator + model.getClassName() + fileType.getFileSuffix());
        });
    }
}
