package com.zj.gencode.manager;

import com.zj.gencode.model.excel.ExcelTable;
import com.zj.gencode.type.FileType;
import org.apache.commons.collections.map.HashedMap;

import java.util.List;
import java.util.Map;

/**
 * 对各种文件输出的接口
 *
 * @author xi.yang
 * @create 2018-12-14 17:46
 **/
public abstract class VmFileManager {
    Map<String,Object> map = new HashedMap();
    public static VmFileManager getInstance(FileType fileType) {
        switch (fileType) {
            case SQL:
                return new VmSqlFileManager();
            case TO:
                return new VmToFileManager();
            case XML:
                return new VmXmlFileManager();
            case MAPPER:
                return new VmMapperFileManager();
            case SERVICE:
                return new VmServiceFileManager();
            case SERVICEIMPL:
                return new VmServiceImplFileManager();
                default:
                    return null;
        }
    }

    public abstract void vmOut(List<ExcelTable> tables);
}
