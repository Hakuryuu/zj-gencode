package com.zj.gencode.model.vm;

import com.zj.gencode.config.GenConfig;
import com.zj.gencode.model.excel.ExcelTable;
import com.zj.gencode.type.FieldType;
import com.zj.gencode.utils.StrUtils;
import com.zj.gencode.utils.bean.BeanUtils;
import lombok.Data;

/**
 * service数据模型
 *
 * @author xi.yang
 * @create 2018-12-14 10:25
 **/
@Data
public class VmServiceModel extends BaseModel {
    private String toPackageName;
    private String daoName;
    private String toName;
    private String fieldName;
    private String fieldType;
    private String lowerToName;
    public VmServiceModel(ExcelTable excelTable) {
        BeanUtils.copyProperties(excelTable, this);
        this.setToName(StrUtils.underline2Camel(excelTable.getTableName(), false));
        this.setLowerToName(StrUtils.underline2Camel(excelTable.getTableName(), true));
        this.setClassName(StrUtils.underline2Camel(excelTable.getTableName(), false));
        this.setToPackageName(GenConfig.instance.getToPackage());
        this.setPackageName(GenConfig.instance.getServicePackage());
        for (ExcelTable.PropField t : excelTable.getPropFields()) {
            if (t.isPrimary()) {
                if (FieldType.ID_ENUM.equals(t.getFieldType()) || FieldType.JAVA_BEAN.equals(t.getFieldType())) {
                    this.fieldType = StrUtils.underline2Camel(t.getFieldName(), false);
                } else {
                    this.fieldType = StrUtils.underline2Camel(t.getFieldType().name(), false);
                }
                this.fieldName = StrUtils.underline2Camel(t.getFieldName(), true);
            }
        }
    }
}


