package com.zj.gencode.model.vm;

import com.zj.gencode.config.GenConfig;
import com.zj.gencode.model.excel.ExcelTable;
import com.zj.gencode.utils.StrUtils;
import com.zj.gencode.utils.bean.BeanUtils;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

/**
 * XML数据模型
 *
 * @author xi.yang
 * @create 2018-12-14 10:25
 **/
@Data
public class VmXmlModel extends BaseModel {
    private List<PropField> propFields;
    private String daoPackageName;
    private String daoName;
    private String toName;
    private String tableName;
    private PropField primaryField;
    public VmXmlModel(ExcelTable excelTable) {
        BeanUtils.copyProperties(excelTable, this);
        this.setToName(StrUtils.underline2Camel(excelTable.getTableName(), false));
        this.setDaoName(this.getToName() + "Dao");
        this.setClassName(StrUtils.underline2Camel(excelTable.getTableName(), false));
        this.setDaoPackageName(GenConfig.instance.getDaoPackage());
        setPropFields(excelTable.getPropFields().stream().map(VmXmlModel.PropField::new).collect(Collectors.toList()));
        for (PropField propField : propFields) {
            if (propField.isPrimary()) {
                this.primaryField = propField;
            }
        }
    }

    @Data
    public class PropField{
        private String sqlFieldName;
        private String fieldName;
        private boolean primary;
        private PropField(ExcelTable.PropField t) {
            this.sqlFieldName = t.getFieldName();
            this.fieldName = StrUtils.underline2Camel(t.getFieldName(), true);
            this.primary = t.isPrimary();
        }
    }
}


