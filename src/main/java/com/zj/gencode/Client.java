package com.zj.gencode;

import com.zj.gencode.config.GenConfig;
import com.zj.gencode.exception.GenException;
import com.zj.gencode.manager.VmFileManager;
import com.zj.gencode.model.excel.ExcelTable;
import com.zj.gencode.type.FieldType;
import com.zj.gencode.type.FileType;
import com.zj.gencode.type.SqlType;
import com.zj.gencode.utils.StrUtils;
import com.zj.gencode.utils.excel.ExcelUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Mr. xi.yang<br/>
 * @version V1.0 <br/>
 * @description: 生成javabean、mybatis配置、sql <br/>
 * @date 2017-09-27 上午 9:33 <br/>
 */
@Slf4j
public class Client {

    private static final Logger logger = LoggerFactory.getLogger(Client.class);
    // 初始化获取系统配置信息并校验参数是否完整
    static {
        GenConfig.init();
    }
    public static void main(String[] args) {
        // 获取excel配置
        List<ExcelTable> tables = getExcelTables();

        // 生成文件
        for (FileType fileType : FileType.values()) {
            VmFileManager vmFileManager = VmFileManager.getInstance(fileType);
            if (null != vmFileManager) {
                vmFileManager.vmOut(tables);
            }
        }
    }


    /**
     * 解析excel转换为对象
     * @return
     */
    private static List<ExcelTable> getExcelTables() {
        List<ExcelTable> result = new ArrayList<>();
        Map<String,List<Map<String, Object>>> maps = ExcelUtil.getAllExcel2Maps(GenConfig.instance.getFileExcelPath());
        for (String sheetName : maps.keySet()) {
            //  2018/12/14 过滤配置页
            if ("items".equalsIgnoreCase(sheetName)) {
                continue;
            }

            ExcelTable table = new ExcelTable();
            List<Map<String,Object>> props = maps.get(sheetName);
            if (props.size() == 0) {
                throw new GenException("Excel配置错误：请至少配置一条数据");
            }
            Map<String, Object> tableNameMap = props.get(0);
            String tableName = String.valueOf(tableNameMap.get("表名"));
            String description = String.valueOf(tableNameMap.get("描述"));
            if (StrUtils.isAnyBlank(tableName,description)) {
                throw new GenException("Excel配置错误：请配置表名和描述信息");
            }
            table.setTableName(tableName);
            table.setDescription(description);
            List<ExcelTable.PropField> list = new ArrayList<>();
            for (Map<String, Object> prop : props) {
                ExcelTable.PropField propField = new ExcelTable.PropField();
                String name = String.valueOf(prop.get("字段名"));
                propField.setFieldName(name);
                String type = String.valueOf(prop.get("类型"));
                propField.setSqlType(SqlType.getSqlTypeByName(type));
                int length = getIntDefault(prop.get("长度"));
                propField.setFieldLength(length);
                boolean allowNull = getBooleanDeDefault(prop.get("允许空"));
                propField.setAllowNull(allowNull);
                String defaultVal = String.valueOf(prop.get("默认值"));
                if(StringUtils.isNotBlank(defaultVal)){
                    propField.setDefaultValue(defaultVal);
                }
                String indexType = String.valueOf(prop.get("索引类型"));
                if(StringUtils.isNotBlank(indexType)){
                    if(indexType.contains("主键")){
                        propField.setPrimary(true);
                    }
                    if(indexType.contains("自增")){
                        propField.setAutoIncrement(true);
                    }
                    if(indexType.contains("索引")){
                        propField.setIndex(true);
                    }
                }
                String content = String.valueOf(prop.get("说明"));
                propField.setContent(content);
                String javaType = String.valueOf(prop.get("Java类型"));
                propField.setFieldType(FieldType.getFieldTypeByName(javaType));
                list.add(propField);
            }
            table.setPropFields(list);
            result.add(table);
        }
        return result;
    }




    private static int getIntDefault(Object obj) {
        if(null == obj){
            return 0;
        }
        try {
            return Integer.parseInt(obj+"");
        }catch (Exception e){
            return 0;
        }
    }
    private static boolean getBooleanDeDefault(Object obj) {
        if(null == obj){
            return false;
        }
        if(StringUtils.isBlank(obj+"")){
            return false;
        }
        if("0".equals(obj+"")){
            return false;
        }
        if("否".equals(obj+"")){
            return false;
        }
        return true;
    }


}
